<img src="/public/screen/react.jpeg">

# 메가커피 키오스크

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

이제 카페에 가지 마세요 <br>
집에서 손쉽게 주문을 해보세요 <br>
마음껏 장바구니에 추가하고 마음껏 주문하시면 됩니다!

### 프로젝트 실행 화면

####  메뉴판
<img src="/public/screen/kiosk-1.png">
원하시는 메뉴를 골라보세요 <br>
메뉴를 클릭하시면 자동으로 장바구니에 담깁니다. <br><br>

####  장바구니
<img src="/public/screen/kiosk-3.png">
클릭하신 제품이 장바구니에 담겼습니다 <br>
삭제를 클릭하면 장바구니에서 삭제되니 걱정은 하지 마세요. <br>
저희가 다 계산해 드릴테니 최소 주문 금액은 걱정하지 마세요!

### 사용한 기술
- 이미지 삽입
- css- flex
- useCallback
- 컴포넌트