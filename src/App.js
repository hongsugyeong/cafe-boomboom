import './App.css';
import DefaultLayout from "./layouts/DefaultLayout";
import {Route, Routes} from "react-router-dom";
import MainScreen from "./pages/MainScreen";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<DefaultLayout><MainScreen /></DefaultLayout>} />
        </Routes>
    </div>
  );
}

export default App;
