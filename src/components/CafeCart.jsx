import React from 'react';

const CafeCart = ({menu, removeFromCart}) => {
    return (
        <div>
            <div>{menu.name}</div>
            <div>{menu.quantity}</div>
            <div>{menu.price * menu.quantity}원</div>
            <button onClick={() => removeFromCart(menu)}>삭제</button>
        </div>
    )
}

export default CafeCart;