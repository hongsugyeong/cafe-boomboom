import React from 'react';

const CafeItem = ({ menu, addToCart }) => {
    return (
        <div onClick={() => {addToCart(menu)}}>
            <img className="mthImgResize" src={process.env.PUBLIC_URL + menu.img} />
            <div>{menu.name}</div>
            <div>{menu.price}원</div>
        </div>
    )
}

export default CafeItem;