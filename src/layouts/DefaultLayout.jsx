import React from 'react';

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>header</div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;