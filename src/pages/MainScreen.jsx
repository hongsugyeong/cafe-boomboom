import React, {useCallback} from "react";
import CafeItem from "../components/CafeItem";
import CafeCart from "../components/CafeCart";

const MainScreen = () => {
    const [cart, setCart] = React.useState([])

    // 목업 데이터임
    const menus = [
        {name: '메가초코', price: 1000, img: '/img/mega-choco.jpg'},
        {name: '아이스 바닐라 라떼', price: 2000, img: '/img/mega-ice-banilalatte.jpg'},
        {name: '메가 아이스티', price: 3000, img: '/img/mega-ice-tea.jpg'},
        {name: '메가리카노', price: 4000, img: '/img/mega-ricano.jpg'},
        {name: '슈크림허니퐁크러쉬', price: 1000, img: '/img/mega-cream-honey-pong.jpg'},
        {name: '망고 요거트스무디', price: 2000, img: '/img/mega-mango-yogurt-smothie.jpg'},
        {name: '큐브라떼', price: 3000, img: '/img/mega-cube-latte.jpg'},
        {name: '메가 카페 모카', price: 4000, img: '/img/mega-cafemoca.jpg'},
    ]

    // 장바구니에 추가하기
    const addToCart = useCallback(menu => {
        setCart((prevCart) => {
            const existingItem = prevCart.find((item) => item.name === menu.name)
            // 장바구니에 존재하는지?
            if (existingItem) {
                // 장바구니에 이미 상품이 존재하는 상태임
                return prevCart.map((item) =>
                    // prevCart map 돌리기
                    // map은 전체를 돌리는 것이기 때문에 이름의 일치와 상관없이 모든 요소들을 확인하고 지나감
                    // 만약에 장바구니에 아아, 아바라가 있을 경우 => 아아를 추가함 => 아아와 이름이 같은지 확인을 해야 되는데 아아뿐만 아니라 아바라까지 확인을 하는 거임
                    item.name === menu.name
                        // 장바구니에 있는 메뉴와 이름이 일치하는가? (= 장바구니에 이미 존재하는 메뉴라면)
                    ? {...item, quantity: item.quantity + 1}
                        // 일치한다면 수량에 +1을 해주기
                        // 만약에 +2로 하면 2개씩 장바구니에 담기는 거임
                    : item
                        // 일치하지 않는다면 그대로 리턴
                )
            } else {
                // 장바구니에 처음 추가하는 경우임
                return [...prevCart, { ...menu, quantity: 1 }]
                // ...을 통해서 값을 기록하는 거임
            }
        })
    }, [])

    // 장바구니에서 삭제하기
    const removeFromCart = useCallback((menu) => {
        setCart((prevCart) => {
            return prevCart.reduce((acc, item) => {
                // reduce: 하나의 결과로 합치기, 누적?
                if (item.name === menu.name) {
                    // 장바구니에 존재하는 거라면
                    if (item.quantity > 1) {
                        // 수량이 1보다 클 경우
                        return [...acc, {...item, quantity: item.quantity - 1}]
                        // 수량 1씩 감소
                        // acc는 해당 제품 이외의 것들을 말하는듯
                    }
                } else {
                    return [...acc, item]
                }
                return acc
            }, [])
        })
    }, [])

    // 총 금액 계산하기
    const calculateTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.quantity, 0)
    }, [cart])

    return (
        <div>
            <h2>메가커피 메뉴</h2>
            <div className="mthFlexCafeMenu">
                {menus.map(menu => (
                    <CafeItem key={menu.name} menu={menu} addToCart={addToCart} />
                    // 함수 원형 자체를 주는 거임, menu는 CafeItem에서 알아서 받음
                ))}
            </div>
            <h2>장바구니</h2>
            {cart.map(menu => (
                <CafeCart key={menu.name} menu={menu} removeFromCart={removeFromCart} />
            ))}
            <h2>총 금액 : {calculateTotal()}원</h2>
        </div>
    )
}

export default MainScreen;